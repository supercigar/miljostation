struct LatLon {
  float LAT, LON, ALT, SIGMA;
  int nSat;
};
struct LatLon Position;
struct HHMMSS {
  int HH, MM, SS;
};
struct HHMMSS Time;
struct DDMMYY {
  int DD, MM, YY;
};
struct DDMMYY Date;


void getRawGPS() {
  while (Serial3.available()) {
    Serial.print((char) Serial3.read());
  }
}

void getGPS() {
  /* Reading of GPS data */
  while (Serial3.available()) {
    serialData = Serial3.read();
    if (gps.encode(serialData)) {
      Position.LAT = gps.location.lat();
      Position.LON = gps.location.lng();
      Position.SIGMA = gps.hdop.value() / 100.0;
      
      Position.ALT = gps.altitude.meters();
      Position.nSat = gps.satellites.value();
      
      Time.HH = gps.time.hour() + 1;
      Time.MM = gps.time.minute();
      Time.SS = gps.time.second();

      Date.DD = gps.date.day();
      Date.MM = gps.date.month();
      Date.YY = gps.date.year();
    }
  }
}
