#include <hpma115S0.h>
#include <SoftwareSerial.h>

int ptcTX = 16; //Particle TX2 for Arduino MEGA
int ptcRX = 17; //Particle RX2 for Arduino MEGA

SoftwareSerial hpmaSerial(ptcTX, ptcRX); //Serial TX/RX pins
HPMA115S0 hpma115S0(hpmaSerial); //Create an instance of the hpma115S0 library

void getParticle() {
  hpmaSerial.begin(9600);
  delay(5000);
  Serial.println("Starting...");
  hpma115S0.Init();
  hpma115S0.StartParticleMeasurement();

  unsigned int pm2_5, pm10;
  if (hpma115S0.ReadParticleMeasurement(&pm2_5, &pm10)) {
    Serial.println("PM 2.5: " + String(pm2_5) + " ug/m3" );
    Serial.println("PM 10: " + String(pm10) + " ug/m3" );
  }
}

