/*  DONT USE DELAY... THE GPS WILL KILL U   >:( */

/* Define where the data should be send
   It's ok to define more than one */
#define Wireless
#define USB


/* Setup of RF transciever ports and serial */
#include <SoftwareSerial.h>
#define RF_Vcc 13
#define RF_EN 12
#define RF_RX 11
#define RF_TX 10
#define RF_AUX 9
#define RF_SET 8
SoftwareSerial RF(RF_TX, RF_RX);

/* variables to track millis and how often to send data */
unsigned long T0 = millis(), T1 = millis();
int dataInterval = 2000; // how often are data send? in ms


/* Setup of GPS package, ports, serial and variables
   LAT is latitude
   LON is longitude
   sigmaPos is the uncertainty in position
   nSat is the number of satellites currently visible
   bog is the input from serial

   !!! USE SERIAL3 AS GPS SERIAL !!! */
#include <TinyGPSPlus.h>
TinyGPSPlus gps;
#define gpsSerial_TX 15
#define gpsSerial_RX 14
int serialData;


/* Setup of DHT11 humidity sensor, pins and variables
   T is temperature
   H is humidity */
#include <dht.h>
dht DHT;
#define HUM 7
int T, TLast, H, HLast;



void setup() {

  Serial.begin(9600);
  Serial3.begin(9600);
  RF.begin(9600);
  pinMode(RF_Vcc, OUTPUT);
  digitalWrite(RF_Vcc, HIGH);

}

void loop() {

  getGPS();
  getTemperature();
  sendData();

}
