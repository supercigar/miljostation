#include <SPI.h>
#include <SD.h>

File file;

void logData() {
  /*Logs the data inside string to SD card in the following folder tree:
    SD CARD
    _year
      _month
        DD-MM-YY.csv
    If given defualt GPS-date (00-00-2000), the function will not log anything and raise an error.
    CSpin is the pin of the CS port on SPI SD card reader.
  */
  int CSPin = 4;

  // Data to be written to the SD card begins here.
  String string;
  /*string += 
  string += String(Time.HH) + ',' + String(Time.MM) + ',' + String(Time.SS);*/
  int time_data[7] = {millis(), Date.DD, Date.MM, Date.YY, Time.HH, Time.MM, Time.SS};
  float gps_data[5] = {(float(Position.nSat)), Position.SIGMA, Position.ALT, (Position.LAT), (Position.LON)};
  int humidity_data[2] = {T,H};
  for (int i = 0; i < 7; i++){
    string += String(time_data[i]) + ',';
  }
  for (int i = 0; i < 5; i++){
    string += String(gps_data[i]) + ',';
  }
  for (int i = 0; i < 2; i++){
    string += String(humidity_data[i]) + ',';
  }
  

  // Data to be written to the SD card ends here.

  int _day = gps.date.day();
  int _month = gps.date.month();
  int _year = gps.date.year();
  Serial.println("SD: logDate() begin");
  String fileformat = ".csv";
  if (_day == 0 and _month == 0 and _year == 2000) { // In case of default GPS date
    Serial.println("SD: Error: Date from GPS is default!");
    //goto error_defaultDate; // Goto end of function.
  } else {
    // Array of the names of the months. Used to create subfolders with the month names on the SD card.
    String months[12] = {"JAN", "FEB", "MAR", "APR", "MAJ", "JUN", "JUL", "AUG", "SEP", "OKT", "NOV", "DEC"};
    String month_name = months[_month - 1]; // Create month name from month number.
    String filename = String(_day) + '-' + String(_month) + '-' + String(_year - 2000) + fileformat;
    String filepath = String(_year) + '/' + month_name + '/' + filename;

    SD.begin(CSPin);
    SD.mkdir(String(_year)); // Year folder is created if not existant.
    if (SD.exists(String(_year)) == true) { // Debugging
      Serial.println("SD: Year folder exists!");
    }

    SD.mkdir((String(_year)) + '/' + month_name); // Month subfolder is created if not existant.
    if (SD.exists((String(_year)) + '/' + month_name) == true) { // Debugging
      Serial.println("SD: Year+month folder exists!");
    }
    File file = SD.open(filepath, FILE_WRITE); // File is opened for writing.
    if (file) { // Debugging
      Serial.print("SD: File \"");
      Serial.print(filepath);
      Serial.println("\" loaded successfully.");
    } else {
      Serial.print("SD: Error: File \"");
      Serial.print(filepath);
      Serial.println("\" did not load!");
    }
    file.println(string);
    file.close(); // File is closed so another file may be opened later.
error_defaultDate:; // Goto-location of defualt GPS date instance.
    Serial.println("SD: logDate() finished");
  }
}
