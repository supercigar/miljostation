void sendData() {
  /* Checking if the enough time has passed */
  T1 = millis();
  if (T1 - T0 > dataInterval) {
    T0 += dataInterval;

    /* print this if code is in RF mode */
#ifdef Wireless
    // GPS data:
    
    RF.println("------------------------------");
    RF.print("Time: "); RF.print(Time.HH); RF.print(" h ");
    RF.print(Time.MM); RF.print("m ");
    RF.print(Time.SS); RF.println("s");
    RF.print("Latitude: "); RF.println(Position.LAT, 6);
    RF.print("Longitude: "); RF.println(Position.LON, 6);
    RF.print("SigmaPos: "); RF.println(Position.SIGMA);
    RF.print("nr. of Sat. : "); RF.println(Position.nSat);
    RF.print("Altitude: "); RF.print(Position.ALT); RF.println(" m");
    RF.println("");

    // Humidity data:
    RF.print("T = "); RF.print(T); RF.println(" *C");
    RF.print("H = "); RF.print(H); RF.println(" %");
    RF.println("------------------------------");
    RF.println("");
#endif

    /* print this if code is in SERIAL mode */
#ifdef USB

    Serial.println("------------------------------");
    // GPS data:
    Serial.print("Time: "); Serial.print(Time.HH); Serial.print("h ");
    Serial.print(Time.MM); Serial.print("m ");
    Serial.print(Time.SS); Serial.println("s");
    Serial.print("Latitude: "); Serial.println(Position.LAT, 6);
    Serial.print("Longitude: "); Serial.println(Position.LON, 6);
    Serial.print("SigmaPos: "); Serial.println(Position.SIGMA);
    Serial.print("nr. of Sat. : "); Serial.println(Position.nSat);
    Serial.print("Altitude: "); Serial.print(Position.ALT); Serial.println(" m");
    Serial.println("");

    // Humidity data:
    Serial.print("T = "); Serial.print(T); Serial.println(" *C");
    Serial.print("H = "); Serial.print(H); Serial.println(" %");
    Serial.println("------------------------------");
    Serial.println("");
#endif
    logData();
    
  }
}
