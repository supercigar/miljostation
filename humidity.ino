void getTemperature() {
  /* Reading and calculating temperature and humidity */

  DHT.read11(HUM);
  T = DHT.temperature;
  T -= 3;
  H = DHT.humidity;

  /* Checking for failed measurements  */
  if (T <= -999) {
    T = TLast;
  }
  if (H <= -999) {
    H = HLast;
  }

  TLast = T;
  HLast = H;
}
